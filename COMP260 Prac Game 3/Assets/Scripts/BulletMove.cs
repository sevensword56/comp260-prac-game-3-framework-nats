﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMove : MonoBehaviour {

    // separate speed and direction so we can
    // tune the speed without changing the code
    public float speed = 10.0f;
    public Vector3 direction;
    public float killTime = 10.0f;
    private float killTimer = 0;
    private Rigidbody rigidbody;

    
    // Use this for initialization
    void Start () {

        rigidbody = GetComponent<Rigidbody>();

    }

    void FixedUpdate()
    {
        rigidbody.velocity = speed * direction;
    }

    void OnCollisionEnter(Collision collision)
    {
        // Destroy the bullet
        Destroy(gameObject);
    }

    // Update is called once per frame
    void Update () {

        killTimer += Time.deltaTime;

        if( killTimer >= killTime)
        {
            Destroy(gameObject);
        }

	}
}
