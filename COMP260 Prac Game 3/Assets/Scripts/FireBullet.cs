﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBullet : MonoBehaviour {

    public BulletMove bulletPrefab;
    public float reload = 0.0f;
    private bool isReloadTime = false;
    private float reloadTimer = 0.0f;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        reloadTimer = reloadTimer + Time.deltaTime;

        if (reloadTimer >= reload)
        {
            isReloadTime = true;

            
        }
        


        // when the button is pushed, fire a bullet
        if (Input.GetButtonDown("Fire1") && isReloadTime == true)
        {
            BulletMove bullet = Instantiate(bulletPrefab);
            // the bullet starts at the player's position
            bullet.transform.position = transform.position;
            // create a ray towards the mouse location
            Ray ray =
            Camera.main.ScreenPointToRay(Input.mousePosition);
            bullet.direction = ray.direction;

            reloadTimer = 0;
            isReloadTime = false;
        }

    }
}
